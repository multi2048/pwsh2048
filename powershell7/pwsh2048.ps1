#!/usr/bin/env pwsh

$BoardState = @((0,0,0,0),(0,0,0,0),(0,0,0,0),(0,0,0,0))
$Score = 0
$Turns = 0

function Paint-BoardBase {
    Clear-Host
    $grid=@('+----+----+----+----+','|    |    |    |    |')
    4..12 | ForEach-Object {
        $host.UI.RawUI.CursorPosition=@{X=5;Y=$_}
        Write-Host $grid[$_%2] -NoNewline
    }
    $host.UI.RawUI.CursorPosition=@{X=0;Y=1}
    Write-Host ' Score:'
    Write-Host ' Turns:'
    $host.UI.RawUI.CursorPosition=@{X=0;Y=14}
    Write-Host ' Controls: ←↓↑→ HJKL 4286'
    Write-Host ' Quit:     Q'
}

function Paint-GameState {
    0..3 | ForEach-Object {
        $r = $_
        0..3 | ForEach-Object {
            $c = $_
            $y = $r*2+5
            $x = $c*5+6
            $host.UI.RawUI.CursorPosition=@{X=$x; Y=$y}
            Write-Host '    ' -NoNewLine # Clear that box
            if ($script:BoardState[$r][$c] -gt 0) {
                $host.UI.RawUI.CursorPosition=@{X=$x; Y=$y}
                Write-Host $script:BoardState[$r][$c] -NoNewLine
            }
        }
    }
    $host.UI.RawUI.CursorPosition=@{X=9; Y=1}
    Write-Host ($script:Score+'    ') -NoNewline
    $host.UI.RawUI.CursorPosition=@{X=9; Y=2}
    Write-Host ($script:Turns+'    ') -NoNewline
}

function Mirror-Board {
    $t = ($script:BoardState | ConvertTo-Json | ConvertFrom-Json)
    $t | ForEach-Object {
        [array]::Reverse($_)
    }
    $script:BoardState = ($t | ConvertTo-Json | ConvertFrom-Json)
}

function Invert-XY {
    $t = @((0,0,0,0),(0,0,0,0),(0,0,0,0),(0,0,0,0))
    0..3 | ForEach-Object {
        $r = $_
        0..3 | ForEach-Object {
            $c = $_
            $t[$c][$r] = $script:BoardState[$r][$c]
        }
    }
    $script:BoardState = ($t | ConvertTo-Json | ConvertFrom-Json)
}

function Push-Left {
    $t = @()
    0..3 | ForEach-Object {
        $t += , @()
        $co = $False
        $script:BoardState[$_] | ForEach-Object {
            $tc = $_
            if ($tc -gt 0) {
                if ((-Not $co) -and ($t[-1].Count -gt 0) -and ($t[-1][-1] -eq $tc)) {
                    $t[-1][-1] = $tc*2
                    $script:Score += $tc*2
                    $co = $True
                } else {
                    $t[-1] = $t[-1] + $tc
                    $co = $False
                }
            }
        }
        while ($t[-1].Count -lt 4) {
            $t[-1] += 0
        }
    }
    $diff = @(Compare-Object $t $script:BoardState -SyncWindow 0).Length
    if ($diff -ne 0) {
        $script:BoardState = ($t | ConvertTo-Json -Compress | ConvertFrom-Json)
        $script:Turns += 1
        Add-NewNumber
    }
}

function Push-Right {
    Mirror-Board
    Push-Left
    Mirror-Board
}

function Push-Up {
    Invert-XY
    Push-Left
    Invert-XY
}

function Push-Down {
    Invert-XY
    Push-Right
    Invert-XY
}

function Add-NewNumber {
    $t = @()
    0..3 | ForEach-Object {
        $r = $_
        0..3 | ForEach-Object {
            $c = $_
            if ($script:BoardState[$r][$c] -eq 0) {
                $t += , @($r,$c)
            }
        }
    }
    $t = $t[(0..($t.Count-1) | Get-Random)]
    if ((1..10 | Get-Random) -eq 1) {
        $n = 4
    } else {
        $n = 2
    }
    $script:BoardState[$t[0]][$t[1]] = $n
}

function Run-2048 {
    $run = $True
    Add-NewNumber
    Add-NewNumber
    Paint-BoardBase
    while ($run) {
        Paint-GameState
        $k=$Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
        $vkc = $k.VirtualKeyCode
        Switch ($vkc) {
            {@(81,27,88).Contains($_)} { $run = $False }
            {@(37,72,52,100).Contains($_)} { Push-Left }
            {@(39,76,54,102).Contains($_)} { Push-Right }
            {@(38,75,56,104).Contains($_)} { Push-Up }
            {@(40,74,50,98).Contains($_)} { Push-Down }
        }
    }
    $host.UI.RawUI.CursorPosition=@{X=0;Y=17}
}

Run-2048
